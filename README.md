# Vaunted - A project by [Finley](https://github.com/FinleyAk) & [Iskawo](https://gitlab.com/Iskawo)

A neat Visual Studio Code Extension to make your Discord.JS bot creating experience a whole load easier

- Added preset `npm run modertion`
- Added preset `npm run fun`
- Added preset `npm run ticket`
