const Discord = require("discord.js");

module.exports = {
    "name": "Ban",
    "triggers": ['ban'],
    "description": "Bans a user from the server",
    "ownerOnly": false,
    "run": async function run(message, client, args) {
        let kUser = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]) || message.guild.bot);
        if (!kUser) return message.channel.send(":x: Please specify a valid \`User\`.");
        if (!message.member.hasPermission("BAN_MEMBERS")) return message.channel.send(":x: **Missing Permission**: \`Ban Members\`.");
        if (kUser.hasPermission("ADMINISTRATOR")) return message.channel.send(":x: Sorry, This user can't be \`Banned\`.");


        if (message.author.id == kUser.id) return message.channel.send("Don't be so hard on yourself");
        message.channel.send("Do you really want to ban this user? Y or N");
        const collector = new Discord.MessageCollector(message.channel, m => m.author.id === message.author.id, {
            time: 10000
        });
        console.log(collector)
        collector.on('collect', message => {

            if (message.content.toLowerCase === "y") {
                message.guild.member(kUser).ban();

                message.channel.send(`:white_check_mark: **Success!** ${kUser} (\`${kUser.id}\`) has been banned.`);

            } else if (message.content.toLowerCase === "n") {
                message.channel.send("User has not been banned.");
            }


        })

    }
};