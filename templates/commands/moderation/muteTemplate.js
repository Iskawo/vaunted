const discord = require("discord.js")
const ms = require("ms");
const fs = require('fs');
// const db = require('quick.db');
module.exports = {
    "name": "Mute",
    "triggers": ['mute'],
    "description": "Mutes a user",
    "ownerOnly": false,
    "run": async function run(message, client) {
        let messageArray = message.content.split(" ");
        let args = messageArray.slice(1);

        // if (!message.member.hasPermission('MUTE_MEMBERS')) {
        //     return message.channel.send('Sorry but you do not have the correct permissions')
        // }
        // if (!message.guild.me.hasPermission("MUTE_MEMBERS")) {
        //     return message.channel.send('Sorry but I do not have the correct permissions')
        // }

        let tomute = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
        if (!tomute) return message.channel.send(":x: Please specify a \`User\`.");
        if (tomute.id == message.author.id) {
            return message.channel.send("Don't be so hard on yourself");
        }


        // if (message.member.highestRole.comparePositionTo(member.highestRole) <= 0) {
        //     return message.channel.send(`${xmark} You can't mute users with equal or higher roles than you.`)
        // }

        if (!message.author.highestRole) {
            if (!muterole) {
                try {
                    muterole = await message.guild.createRole({
                        name: "Muted",
                        color: "#95A5A6",
                        permissions: []
                    })
                    message.guild.channels.forEach(async (channel, id) => {
                        await channel.overwritePermissions(muterole, {
                            SEND_MESSAGES: false,
                            ADD_REACTIONS: false
                        });
                    });
                } catch (e) {
                    console.log(e.stack);
                }
            }

            let mutetime = args[1];
            if (!mutetime) {
                tomute.addRole(muterole.id)
                message.channel.send(`:white_check_mark: ${tomute} (\`${tomute.id}\`) **has been muted, permanently**`)

            }

            if (tomute.roles.has("name", "Muted")) return message.channel.send(`:x: ${tomute.user.username}, user is already muted, changing time to ${ms(ms(mutetime))}`)

            await (tomute.addRole(muterole.id));
            message.channel.send(`:white_check_mark: ${tomute} (\`${tomute.id}\`) **has been muted for** \`${ms(ms(mutetime))}\``);

            setTimeout(function () {
                tomute.removeRole(muterole.id);
            }, ms(mutetime));
        } else {
            message.channel.send("Sorry I can not mute them.")
        }
    }
}