const discord = require("discord.js")
const ms = require("ms");
module.exports = {
    "name": "Unmute",
    "triggers": ['unmute'],
    "description": "Unmutes a user",
    "ownerOnly": false,
    "run": async function run(message, args, client) {

        if (message.member.hasPermission("MANAGE_MESSAGES")) {
            let muteRole = message.guild.roles.find("name", "Muted");
            if (!muteRole) {
                message.channel.send("Sorry, there's no Muted role in this guild.")
            }
            let member = message.mentions.members.first();
            if (!member) return message.channel.send("Please give me a user to unmute");

            if (!member.roles.find("name", "Muted")) return message.channel.send(`:x: ${member.user.username}, isn't muted!`)

            member.removeRole(muteRole.id);
            message.channel.send(`:white_check_mark: \`${member.user.username} (${message.member.id})\` has been __**unmuted**__`);
        } else {
            message.channel.send("Sorry, you don't have the required permissions!")
        }
    }

}